1) Create a a mysql database called task_db, where project data will be persisted
-Attached PDF file shows the database structure and relationships

2)Unzipp the project and open in your command prompt or terminal

3) Run the following commands 
Note:before you run the command make sure that laravel development is set either 
globally or to current command prompt or terminal

i)php artisan migrate
 this command creates database tables required by the project to run successfully
ii) php artisan db:seed 
 this command seeds priorities into the priorities table as [High , Medium and Low]

The above two commands can combined into one by running
 php artisan migrate --seed
 it creates and seeds

4)php artisan serve
 this command starts the development server
Mine is started at: http://127.0.0.1:8000 yours can be different please note it

5)Copy that url into you prefered browser to load the project ui

6)Register an account and then login
 -Start by creating project followed tasks 